Role-resgister-cluster-in-postgres
=========

The role inserts or updates a cluster in the postgresql db.
The postgres has two tables - cloudlets and cloudlets_certificates, the role works with both.



Requirements
------------

the python script uses these libraries :
 - psycopg2
 - sys
 - datetime


Role Variables
--------------

Most of the variables are supposed to be set in the inventory, but the last six are set by cluster-certs-info role

| variable        | required | default | choices | comments                                                                 |
|-----------------|----------|---------|---------|--------------------------------------------------------------------------|
| db_user         | yes      |         |         | postgres user from inventory                                             |
| db_pass         | yes      |         |         | postgres password from inventory                                         |
| db_host         | yes      |         |         | postgres host from inventory                                             |
| cluster_name    | yes      |         |         | inventory                                                                |
| base_domain     | yes      |         |         | inventory                                                                |
| current_ocp_version | yes  |         |         | taken from within this role in the first task "getting version"          |
| location        | yes      |         |         | inventory                                                                |
| platform        | yes      |         |         | inventory                                                                |
| type            | yes      |         |         | inventory                                                                |
| vc_url          | yes      |         |         | inventory                                                                |
| pikud           | yes      |         |         | inventory                                                                |
| issuer_api      | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |
| subject_api     | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |
| expiration_api  | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |
| issuer_apps     | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |
| subject_apps    | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |
| expiration_apps | yes      |         |         | cluster-certs-info role (post job workflow) or manually add to inventory |

Dependencies
------------

This role is ran as part of the **Post Jobs Workflow** on ansible tower.
The role *cluster-certs-info* sets six parameters which are mandatory for this script to run fully.
Look at the variables table above to see which variable is being set through the workflow and which is part of the inventory.
BTW you can manually add these specific values to the inventory if you wish to execute the role separately, I did it and it worked fine (Nir Levy)
this role also need the be ran after "role-copy-kubeconfig", in order to get to current ocp version


Example Playbook
----------------

https://gitlab.com/ananon/ansible/register-cluster-in-postgres/
```yaml
---
- name: Register deployed cluster in our postgres DB
  hosts: localhost
  become: false
  gather_facts: false
  roles:
    - role-register-cluster-in-postgres

```


Python Code Overview
--------------------

## the main

```python
if __name__ == '__main__':
    # insert one cloudlet
    insert_cloudlet(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9], timestamp.strftime("%c"), "https://"+sys.argv[10], sys.argv[11])
    # insert cloudlet certificates
    insert_cloudlet_certs(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[12], sys.argv[13], sys.argv[14], sys.argv[15], sys.argv[16], sys.argv[17])
```
two functions are being used, insert_cloudlet and insert_cloudlet_certs.
the order is important because in order to insert to the cloudlet_certificates table the cloudlet has to be registered first in the cloudlets table, 
so the query can use its id from the other table that is how we pair the rows on both tables.

## insert_cloudlet
inserts a new cloudlet to the cloudlets table using these queries:
```python
    insert_query = """INSERT INTO cloudlets(name ,base_domain, version, location, platform, type, creation_date,pikud ,vc_url)
             VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id;"""
    exists_query = """ select EXISTS ( select 1 from cloudlets where name = %s ); """

    update_query = """UPDATE cloudlets
        SET base_domain = %s,
        version = %s,
        location = %s,
        platform = %s,
        type = %s,
        pikud = %s,
        vc_url = %s
        WHERE name = %s;"""
```
after connecting to the postgres db, it checks if the cloudlet exists, if it does, it will execute the update_query,
otherwise it will use the insert_query.

## insert_cloudlet_certs
inserts a new cloudlet to the cloudlets_certificates table using these queries:
```python
    insert_query = """INSERT INTO cloudlets_certificates(issuer_api, subject_api, expiration_api, issuer_apps,
     subject_apps, expiration_apps, cluster_id)
             VALUES(%s,%s,%s,%s,%s,%s,%s) RETURNING id;"""
    extract_cluster_id = """ select id from cloudlets where name = %s ; """
    exists_query = """ select EXISTS ( select 1 from cloudlets_certificates, cloudlets where cloudlets_certificates.cluster_id = %s ); """
    update_query = """UPDATE cloudlets_certificates
        SET issuer_api = %s,
        subject_api = %s,
        expiration_api = %s,
        issuer_apps = %s,
        subject_apps = %s,
        expiration_apps = %s
        WHERE cluster_id = %s;"""
```
after connecting to the postgres db, it gets the id of the cloudlet from the *cloudlets* table:
```python
        # execute theextract_cluster_id
        cur.execute (extract_cluster_id, (name,))
        cluster_id = cur.fetchone()[0]
        # execute the EXISTS statement
        cur.execute (exists_query, (cluster_id,))
        # get boolean value
        row_exists = cur.fetchone()[0]
```
then it checks if it exists in the *cloudlets_certificates* table and decides if it should insert a new cloudlet or update one.
