import json
import sys
import datetime
import psycopg2
# 1169
# Take timestamp
timestamp = datetime.datetime.now()


def insert_cloudlet_certs(user, password, host, name, issuer_api, subject_api, expiration_api, issuer_apps, subject_apps, expiration_apps):
    insert_query = """INSERT INTO cloudlets_certificates(issuer_api, subject_api, expiration_api, issuer_apps,
     subject_apps, expiration_apps, cluster_id)
             VALUES(%s,%s,%s,%s,%s,%s,%s) RETURNING id;"""
    extract_cluster_id = """ select id from cloudlets where name = %s ; """
    exists_query = """ select EXISTS ( select 1 from cloudlets_certificates, cloudlets where cloudlets_certificates.cluster_id = %s ); """
    update_query = """UPDATE cloudlets_certificates
        SET issuer_api = %s,
        subject_api = %s,
        expiration_api = %s,
        issuer_apps = %s,
        subject_apps = %s,
        expiration_apps = %s
        WHERE cluster_id = %s;"""
    conn = None
    id = None
    try:
        # read database configuration
        params = {
            "dbname": "postgres",
            "user": user,
            "host": host,
            "password": password,
            "port": "5432",
            "sslmode": "require",
            }
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute theextract_cluster_id
        cur.execute (extract_cluster_id, (name,))
        cluster_id = cur.fetchone()[0]
        # execute the EXISTS statement
        cur.execute (exists_query, (cluster_id,))
        # get boolean value
        row_exists = cur.fetchone()[0]

        if not row_exists:
            # execute the INSERT statement
            cur.execute(insert_query, (issuer_api, subject_api, expiration_api, issuer_apps, subject_apps, expiration_apps, cluster_id,))
            # get the generated id back
            id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            print(name + " inserted to DB as id: " + str(id))
        else:
            print(name + " already exists in cloudlets_certificates table, updating.")
            cur.execute(update_query,(issuer_api, subject_api, expiration_api, issuer_apps, subject_apps, expiration_apps, cluster_id,))
            # commit the changes to the database
            conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def insert_cloudlet(user, password, host, name, base_domain, version, location, platform, type, creation_date, vc_url, pikud):
    """ insert a new cloudlet into the cloudlets table """
    insert_query = """INSERT INTO cloudlets(name ,base_domain, version, location, platform, type, creation_date,pikud ,vc_url)
             VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id;"""
    exists_query = """ select EXISTS ( select 1 from cloudlets where name = %s ); """

    update_query = """UPDATE cloudlets
        SET base_domain = %s,
        version = %s,
        location = %s,
        platform = %s,
        type = %s,
        pikud = %s,
        vc_url = %s
        WHERE name = %s;"""
    conn = None
    id = None
    try:
        # read database configuration
        params = {
            "dbname": "postgres",
            "user": user,
            "host": host,
            "password": password,
            "port": "5432",
            "sslmode": "require",
            }
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the EXISTS statement
        cur.execute(exists_query, (name,))
        # get boolean value
        row_exists = cur.fetchone()[0]

        if not row_exists:
            # execute the INSERT statement
            cur.execute(insert_query, (name, base_domain, version, location, platform, type, creation_date, pikud, vc_url,))
            # get the generated id back
            id = cur.fetchone()[0]
            # commit the changes to the database
            conn.commit()
            print(name + " inserted to DB as id: " + str(id))
        else:
            # if row exists, update its columns
            print(name + " already exists in cloudlets table in DB, updating.")
            cur.execute(update_query, (base_domain, version, location, platform, type, pikud, vc_url, name,))
            # commit the changes to the database
            conn.commit()

        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    # insert one cloudlet
    insert_cloudlet(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9], timestamp.strftime("%c"), "https://"+sys.argv[10], sys.argv[11])
    # insert cloudlet certificates
    insert_cloudlet_certs(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[12], sys.argv[13], sys.argv[14], sys.argv[15], sys.argv[16], sys.argv[17])