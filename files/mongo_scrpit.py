import urllib.parse
import sys
import dns
from pymongo import MongoClient
import datetime

# Connection Vars
mongoUser = sys.argv[1]
mongoPass = sys.argv[2]
mongoServer = sys.argv[3]

# Connect to DB
client = MongoClient("mongodb+srv://" + mongoUser+ ":" + urllib.parse.quote( mongoPass ) + "@" + mongoServer)
db = client['cloudlet'] 
collection = db['clusters'] 

# Take timestamp
timestamp = datetime.datetime.now()

# document defintion
dataDict =	{
  "name": sys.argv[4],
  "base_domain": sys.argv[5],
  "version": sys.argv[6],
  "location": sys.argv[7],
  "platform": sys.argv[8],
  "type": sys.argv[9],
  "creation_date": timestamp.strftime("%c")
 
}

# Insert document
collection.insert(dataDict) 
client.close() 
